const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const fetch = require('node-fetch');

require('dotenv').config();

const middlewares = require('./middlewares');
const api = require('./api');
const vote = require('./pages/vote');

const app = express();

app.use(morgan('dev'));
app.use(helmet());
app.use(cors());
app.use(express.json());

app.set('view-engine', 'ejs');

app.get('/', (req, res) => {
  fetch('https://icanhazdadjoke.com', {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'GET',
  }).then(async (response) => {
    const jVal = await response.json();
    const { joke } = jVal;
    res.render('dadjoke.ejs', { joke });
  });
});


app.use('/api', api);
app.use('/', vote);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
