const express = require('express');

const bodyParser = require('body-parser');
const webhook = require('webhook-discord');

const Hook = new webhook.Webhook(
  'https://discordapp.com/api/webhooks/753282104028168225/9o0R-kB1XZXlBPetBmNV7EbDXaqZvZxL8DW11SDA83Mixutu53TYmeUZvp8NNwz78MCh'
);

const router = express.Router();

router.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

router.use(bodyParser.json());

router.post('/', async (req, res) => {
  const { name, name_other, why, contact } = req.body;
  const msg = new webhook.MessageBuilder()
    .setName('Agonia Stämningar')
    .setColor('#aabbcc')
    .setTitle(`${name}`)
    .setDescription(
      `\n**Stämmer:** ${name_other}\n\n**Av anledning:** ${why}\n\n **Kontakt:** ${contact}`
    );
  Hook.send(msg);
  res.status(200).send(req.body);
});

module.exports = router;
