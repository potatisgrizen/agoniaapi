const express = require('express');

// Import API
const faqs = require('./faqs');
//const sue = require('./sue');

const router = express.Router();

router.get('/', (req, res) => {
  res.status(200).send({status: 200});
});

router.use('/faqs', faqs);
//router.use('/sue', sue);

module.exports = router;
