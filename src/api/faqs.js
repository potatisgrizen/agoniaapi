const express = require('express');
const monk = require('monk');
const Joi = require('joi');

const db = monk(process.env.MONGO_URI);
const faqs = db.get('FAQ');

const schema = Joi.object({
  question: Joi.string().trim().required(),
  answer: Joi.string().trim().required(),
});

const router = express.Router();

// Read all
router.get('/', async (req, res, next) => {
  try {
    const items = await faqs.find({});
    res.json(items);
  } catch (error) {
    next(error);
  }
});

// Read one
router.get('/:id', async (req, res, next) => {
  const { id } = req.params;
  const item = await faqs.findOne({
    _id: id,
  });
  if (!item) return next();
  return res.json(item);
});

// Create one
router.post('/', async (req, res, next) => {
  try {
    const value = await schema.validateAsync(req.body);
    const inserted = await faqs.insert(value);
    res.json(inserted);
  } catch (error) {
    next(error);
  }
});

// Update One
router.put('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const value = await schema.validateAsync(req.body);
    const item = await faqs.findOne({
      _id: id,
    });
    if (!item) return next();
    await faqs.update(
      {
        _id: id,
      },
      {
        $set: value,
      },
    );
    res.json(value);
  } catch (error) {
    next(error);
  }
});

// Delete one
router.delete('/:id', async (req, res, next) => {
  const { id } = req.params;
  const item = await faqs.findOne({
    _id: id,
  });
  if (!item) return next();
  await faqs.remove({ _id: id });
  res.status(200).send({ Message: 'Success' });
});

module.exports = router;
