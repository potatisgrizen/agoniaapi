const express = require('express');
const fetch = require('node-fetch');

const monk = require('monk');

const db = monk(process.env.MONGO_LOGIN);
const login = db.get('Login');

const router = express.Router();

const CLIENT_ID = '750783054024409210';
const CLIENT_SECRET = 'VVWY9fqrEyyctQG3lw2VcATfkrYrBT89';
const REDIRECT_URI = 'http://localhost:5000/vote/';

router.get('/login', (req, res) => {
  res.render('discordlogin.ejs');
});

router.get('/vote', (req, res) => {
  const code = req.query.code;
  if (!code) return;
  const data = {
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
    grant_type: 'authorization_code',
    redirect_uri: REDIRECT_URI,
    code: code,
    scope: 'identify guilds',
  };
  fetch('https://discord.com/api/oauth2/token', {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    method: 'POST',
    body: new URLSearchParams(data)
  }).then(async (response) => {
    const jVal = await response.json();
    await login.insert(jVal);
  });
  
  res.render('vote.ejs');
});

module.exports = router;